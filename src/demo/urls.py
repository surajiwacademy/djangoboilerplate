from django.conf import settings
import debug_toolbar
from django.contrib import admin
from django.urls import path, include

from .views import home

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home, name="home"),
    path('accounts/', include('allauth.urls')),
]
if settings.DEBUG:

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),


    ] + urlpatterns
